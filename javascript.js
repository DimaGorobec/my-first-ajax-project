const users = document.querySelector('.users')
const usersInfo = document.querySelector('.user_info')
class User {
	constructor (id, name, email,username,address, company, phone,website) {
		this.id = id;
		this.name = name;
		this.node = this.createNode();
		this.email = email;
		this.username = username;
		this.address = address;
		this.company = company;
		this.phone = phone;
		this.website = website;
		this.createNodeInfo = this.createNodeInfo.bind(this)

	}
	createNode() {
		let div = document.createElement('div')
		let photo = document.createElement('div');
		photo.classList.add('photo');
		let h3 = document.createElement('h3');
		h3.textContent = this.name
		h3.addEventListener('click',this.createNodeInfo.bind(this))
		div.appendChild(photo);
		div.appendChild(h3);
		users.appendChild(div)
		return div
		}
	// this.createNodeInfo.bind(this)
		 createNodeInfo() {
console.log(this)
		usersInfo.innerHTML = '';
		let h3 = document.createElement('h3');
		h3.textContent = this.name;
		let photo = document.createElement('div');
		photo.classList.add('photo');
		let divConteiner = document.createElement('div');
		divConteiner.innerHTML = '';
		let username = document.createElement('p');
		username.textContent = 'username :' + this.username;
		let email = document.createElement('p');
		email.textContent = 'email : ' + this.email
		let address = document.createElement('ul');
			address.textContent = 'Address :'
			let street = document.createElement('li')
			street.textContent = 'street : ' + this.address.street
			address.appendChild(street)
			// console.log(street)
			let suite = document.createElement('li')
			suite.textContent = 'suite : ' + this.address.suite
			address.appendChild(suite)
			let city = document.createElement('li')
			city.textContent = 'city : ' + this.address.city
			address.appendChild(city)
			let zipcode = document.createElement('li')
			zipcode.textContent = 'zipcode : ' + this.address.zipcode
			address.appendChild(zipcode)
		let phone = document.createElement('p');
		phone.textContent = 'phone : ' + this.website;
		let company = document.createElement('ul');
			company.textContent = 'Company :'
			let nameCompany = document.createElement('li')
			nameCompany.textContent = 'Name : ' + this.company.name
			company.appendChild(nameCompany)
			let catchPhrase = document.createElement('li')
			catchPhrase.textContent = 'CatchPhrase : ' + this.company.catchPhrase
			company.appendChild(catchPhrase)
			let bs = document.createElement('li')
			bs.textContent = 'Bs : ' + this.company.bs
			company.appendChild(bs)

		divConteiner.appendChild(photo);
		divConteiner.appendChild(h3);
		divConteiner.appendChild(username)
		divConteiner.appendChild(email)
		divConteiner.appendChild(address)
		divConteiner.appendChild(phone)
		divConteiner.appendChild(company)
		usersInfo.appendChild(divConteiner)	
		return divConteiner
	}
	
}



function createUsers(list) {
	console.log(list)
	list.forEach((user) => {
		let myUsers = new User(user.id, user.name, user.email, user.username,
			user.address,user.company,user.street,user.phone,user.website, user.body)
		users.appendChild(myUsers.node)
	});
}

function createUsersInfo() {

}

function init () {
	fetch('https://jsonplaceholder.typicode.com/users ')
		.then((response) => {return response.json();})
		.then((data) => {
			console.log(data)
			createUsers(data)
		})	
}
init()


